
#ifndef MAIN_H
#define MAIN_H

#include <Godot.hpp>
#include <Node.hpp>

class Main : public godot::Node {
	GODOT_CLASS(Main, godot::Node)

public:
    void _init() {}
	void _ready();

	static void _register_methods();
};

#endif // MAIN_H
