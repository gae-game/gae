#ifndef MENU_H
#define MENU_H

#include <Godot.hpp>
#include <Node.hpp>
#include <Tree.hpp>
#include <TreeItem.hpp>

class Menu : public godot::Node {
	GODOT_CLASS(Menu, godot::Node)

	godot::Tree *_navigation_tree;
    godot::TreeItem *_playBtn;
    godot::TreeItem *_createGameBtn;
    godot::TreeItem *_findGameBtn;
    godot::TreeItem *_settingsBtn;

public:
    void _init() {}
	void _ready();

    static void _register_methods();
};

#endif // MENU_H
