#include "menu.hpp"

#include <SceneTree.hpp>


void Menu::_ready() {
	_navigation_tree = get_node<godot::Tree>("HBoxContainer/NavigationTree");

	godot::TreeItem *root = _navigation_tree->create_item();
    _navigation_tree->set_hide_root(true);
    _playBtn = _navigation_tree->create_item(root);
    _createGameBtn = _navigation_tree->create_item(_playBtn);
    _findGameBtn = _navigation_tree->create_item(_playBtn);
    _settingsBtn = _navigation_tree->create_item(root);

    _playBtn->set_text(0, "Play");
    _createGameBtn->set_text(0, "Create Game");
    _findGameBtn->set_text(0, "Find Game");
    _settingsBtn->set_text(0, "Settings");

}

void Menu::_register_methods() {
    godot::register_method("_ready", &Menu::_ready);
}
