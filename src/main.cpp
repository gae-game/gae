#include "main.hpp"

#include <SceneTree.hpp>

void Main::_ready() {
}

void Main::_register_methods() {
	godot::register_method("_ready", &Main::_ready);
}
